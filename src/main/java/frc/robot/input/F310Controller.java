// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.input;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.POVButton;

public class F310Controller extends GenericHID {

  /** Represents a digital button on an F310 Controller. */
  private enum Button {
    kA(1),
    kB(2),
    kX(3),
    kY(4),
    kLb(5),
    kRb(6),
    kBack(7),
    kStart(8),
    kLs(9),
    kRs(10);

    public final int value;

    Button(int value) {
      this.value = value;
    }
  }

  /** Represents an axis on an F310 Controller. */
  public enum Axis {
    kLeftX(0),
    kLeftY(1),
    kLt(2),
    kRt(3),
    kRightX(4),
    kRightY(5);

    public final int value;

    Axis(int value) {
      this.value = value;
    }
  }

  /** Reprents a hand on an F310 Controller */
  public enum Hand {
    kLeft,
    kRight;
  }

  // Joystick Buttons

  public final JoystickButton buttonA = new JoystickButton(this, Button.kA.value);
  public final JoystickButton buttonB = new JoystickButton(this, Button.kB.value);
  public final JoystickButton buttonX = new JoystickButton(this, Button.kX.value);
  public final JoystickButton buttonY = new JoystickButton(this, Button.kY.value);
  public final JoystickButton buttonLb = new JoystickButton(this, Button.kLb.value);
  public final JoystickButton buttonRb = new JoystickButton(this, Button.kRb.value);
  public final JoystickButton buttonBack = new JoystickButton(this, Button.kBack.value);
  public final JoystickButton buttonStart = new JoystickButton(this, Button.kStart.value);
  public final JoystickButton buttonLs = new JoystickButton(this, Button.kLs.value);
  public final JoystickButton buttonRs = new JoystickButton(this, Button.kRs.value);

  // Joystick Axes

  public final JoystickAxis axisLeftX = new JoystickAxis(this, Axis.kLeftX.value);
  public final JoystickAxis axisLeftY = new JoystickAxis(this, Axis.kLeftY.value);
  public final JoystickAxis axisLt = new JoystickAxis(this, Axis.kLt.value);
  public final JoystickAxis axisRt = new JoystickAxis(this, Axis.kRt.value);
  public final JoystickAxis axisRightX = new JoystickAxis(this, Axis.kRightX.value);
  public final JoystickAxis axisRightY = new JoystickAxis(this, Axis.kRightY.value);

  // Joystick POVs

  public final POVButton povUp = new POVButton(this, 0);
  public final POVButton povRight = new POVButton(this, 90);
  public final POVButton povDown = new POVButton(this, 180);
  public final POVButton povLeft = new POVButton(this, 270);

  /**
   * Construct an instance of a joystick. The joystick index is the USB port on the drivers station.
   *
   * @param port The port on the Driver Station that the joystick is plugged into.
   */
  public F310Controller(final int port) {
    super(port);
  }

  /**
   * Get the X axis value of the controller.
   *
   * @param Hand Side of controller whose value should be returned.
   * @return The X axis value of the controller.
   */
  public double getIntensityX(Hand hand) {
    return hand.equals(Hand.kLeft) ? getRawAxis(Axis.kLeftX.value) : getRawAxis(Axis.kRightX.value);
  }

  /**
   * Get the Y axis value of the controller. This is negated such that +Y is up.
   *
   * @param hand Side of controller whose value should be returned.
   * @return The Y axis value of the controller.
   */
  public double getIntensityY(Hand hand) {
    return -(hand.equals(Hand.kLeft)
        ? getRawAxis(Axis.kLeftY.value)
        : getRawAxis(Axis.kRightY.value));
  }

  /**
   * Get the trigger axis value of the controller.
   *
   * @param hand Side of controller whose value should be returned.
   * @return The trigger axis value of the controller.
   */
  public double getIntensityTriggerAxis(Hand hand) {
    return hand.equals(Hand.kLeft) ? getRawAxis(Axis.kLt.value) : getRawAxis(Axis.kRt.value);
  }
}
