// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import io.github.oblarg.oblog.annotations.Log;

import org.photonvision.PhotonCamera;

/**
 * Represents the vision subsystem.
 *
 * <p>The vision subsystem encompasses managing cameras attached to the roboRIO, and interfacing
 * with coprocessors to provide object tracking data from the other camera.
 */
public class VisionSubsystem extends SubsystemBase {

  // PhotonLib Camera Class
  private PhotonCamera m_photonCamera = new PhotonCamera("longwood564");
  private UsbCamera m_usbCamera = CameraServer.startAutomaticCapture(0);

  @Log(name = "Cargo Target Exists")
  private boolean m_hasTargets = false;

  // Current distance to target.
  private double m_distanceToTarget;

  // Current pitch to target.
  private double m_yawToTarget;

  // TODO: Resolve PhotoVision camera issues.
  // /** This method is run periodically. */
  // @Override
  // public void periodic() {
  //   // Get the latest pipeline result.
  //   PhotonPipelineResult pipelineResult = m_photonCamera.getLatestResult();

  //   // Update the telemetry.
  //   m_hasTargets = pipelineResult.hasTargets();

  //   // Update the best target.
  //   if (m_hasTargets) {
  //     PhotonTrackedTarget target = pipelineResult.getBestTarget();
  //     m_yawToTarget = Units.degreesToRadians(target.getYaw());
  //     m_distanceToTarget =
  //         PhotonUtils.calculateDistanceToTargetMeters(
  //             // Height of camera off floor in meters.
  //             0.5,
  //             FieldConstants.kCargoDiameter,
  //             // The pitch of the intake camera in meters.
  //             0.5,
  //             Units.degreesToRadians(target.getPitch()));
  //     SmartDashboard.putNumber("Target Area", target.getArea());
  //   } else {
  //     m_distanceToTarget = 0;
  //     m_yawToTarget = 0;
  //     SmartDashboard.putNumber("Target Area", 0);
  //   }
  //   SmartDashboard.putNumber("Distance to Target", m_distanceToTarget);
  //   SmartDashboard.putNumber("Yaw to Target", m_yawToTarget);
  // }

  /** @return Whether targets have been found. */
  public boolean hasTargets() {
    return m_hasTargets;
  }

  /** @return The latest distance to the target, where 0 is returned if none found. */
  public double getDistanceToTarget() {
    return m_distanceToTarget;
  }

  /** @return The latest yaw to the target, where 0 is returned if none found. */
  public double getYawToTarget() {
    return m_yawToTarget;
  }
}
